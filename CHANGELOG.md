# CHANGELOG

## 0.5.1
- Write markdown keywords as H2 (previously, they were being written as H1)

## 0.5.0
- Sort the files
- Add `-D` short argument for description

## 0.4.2
- Make the LaTeX build properly

## 0.4.1
- Fix console script

## 0.4.0
- Fix package creation

## 0.3.3
- Add horizontal arrows and dashes (–, —, ―, ‒)

## 0.3.2
- European UTF-8 letters are escaped, so they don't cause crashes

## 0.3.1
- Line is not quoted, if it is a keyword line

## 0.3.0
- Support for PDF color schemes

## 0.2.1
- Removed `accsupp` package, which was returning errors
- Updated README

## 0.2.0
- Support for PDF and TEX output

## 0.1.2
- Fix README
- Add CHANGELOG
