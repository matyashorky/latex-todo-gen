# Color schemes

You can specify color scheme for generated PDF file.

## light

The default. Black, grey comments and purple keywords.

![light](light.jpg)

## plain

Black and white only. Useful for printing.

![plain](plain.jpg)

## mariana

Mariana color scheme. Green-ish grey background, white text, green comments and purple keywords.

![mariana](mariana.jpg)

## marianne

Slightly edited mariana, for ease of reading.

![marianne](marianne.jpg)

## dark

Black background, white text. Green comments and purple keywords.

![dark](dark.jpg)
